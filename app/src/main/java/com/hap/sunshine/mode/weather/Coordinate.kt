package com.hap.sunshine.mode.weather

/**
 * Created by elpolvo on 6/8/17.
 */

class Coordinate(val lon: Double, val lat: Double)
