package com.hap.sunshine.mode.weather

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by elpolvo on 6/8/17.
 */

class Description(@SerializedName("dt")
                  val timestamp: Long, @SerializedName("temp")
                  val temperature: Temperature, val pressure: Double, val humidity: Int, val weather: ArrayList<Weather>, val speed: Double, val deg: Double, val clouds: Int)
