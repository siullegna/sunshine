package com.hap.sunshine.mode.weather

import java.util.*

/**
 * Created by elpolvo on 6/8/17.
 */

class WeatherResponseContainer(val city: City, val list: ArrayList<Description>)
