package com.hap.sunshine.mode.weather

/**
 * Created by elpolvo on 6/8/17.
 */

class Temperature(val day: Double, val min: Double, val max: Double, val eve: Double, val morn: Double)
