package com.hap.sunshine.mode.weather

/**
 * Created by elpolvo on 6/8/17.
 */

class City(val id: Long, val name: String, val coord: Coordinate, val country: String, val population: Long)
