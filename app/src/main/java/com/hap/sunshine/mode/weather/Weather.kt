package com.hap.sunshine.mode.weather

/**
 * Created by elpolvo on 6/8/17.
 */

class Weather(val id: Long, val main: String, val description: String, val icon: String)
