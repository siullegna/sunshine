package com.hap.sunshine.mode

/**
 * Created by elpolvo on 6/8/17.
 */

enum class CountryCode private constructor(val countryCode: String) {
    US("us"),
    MX("mx")
}
