package com.hap.sunshine

import android.app.Application
import butterknife.ButterKnife
import com.hap.sunshine.dagger2.annotation.module.AppComponentModule
import com.hap.sunshine.dagger2.component.AppComponent
import com.hap.sunshine.dagger2.component.DaggerAppComponent

/**
 * Created by luis on 4/28/17.
 */

class SunshineApplication : Application() {
    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        ButterKnife.setDebug(true)

        component = DaggerAppComponent
                .builder()
                .appComponentModule(AppComponentModule(this))
                .build()

        component.inject(this)
    }
}