package com.hap.sunshine.network

import com.hap.sunshine.mode.weather.WeatherResponseContainer
import dagger.Module

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by luis on 4/30/17.
 */
@Module
interface WeatherRestApi {
    @GET("/data/2.5/forecast/daily?cnt={cnt}&q={zipCode},{countryCode}&mode={mode}&units={units}&appid={appid}")
    fun getWeather(@Query("cnt") dayCount: Int,
                   @Query("zipCode") zipCode: Long,
                   @Query("countryCode") countryCode: String,
                   @Query("mode") mode: String,
                   @Query("units") units: String,
                   @Query("appid") appid: String): Observable<WeatherResponseContainer>
}