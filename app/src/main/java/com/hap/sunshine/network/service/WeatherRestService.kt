package com.hap.sunshine.network.service

import com.hap.sunshine.mode.weather.WeatherResponseContainer
import com.hap.sunshine.network.WeatherRestApi
import io.reactivex.Observable

/**
 * Created by elpolvo on 6/8/17.
 */
class WeatherRestService(val weatherRestApi: WeatherRestApi) {
    private val mode: String = "json"
    private val appid: String = "cda4d685f66cd787cc17b2ea43649c6a"

    private fun getWeather(dayCount: Int, zipCode: Long, countryCode: String, mode: String, units: String, appid: String): Observable<WeatherResponseContainer> {
        return weatherRestApi.getWeather(dayCount, zipCode, countryCode, mode, units, appid)
    }

    fun getWeather(dayCount: Int, zipCode: Long, countryCode: String, units: String): Observable<WeatherResponseContainer> {
        return getWeather(dayCount, zipCode, countryCode, mode, units, appid)
    }
}
