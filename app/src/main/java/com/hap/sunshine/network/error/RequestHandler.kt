package com.hap.sunshine.network.error

import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import java.lang.reflect.Type

/**
 * Created by elpolvo on 6/8/17.
 */
class RequestHandler : CallAdapter.Factory {
    val original: RxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create();

    constructor() : super()

    companion object {
        @JvmStatic fun create(): RequestHandler {
            return RequestHandler()
        }
    }

    override fun get(returnType: Type?, annotations: Array<out Annotation>?, retrofit: Retrofit?): CallAdapter<*, *> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

//    private class RxHandlerAdapterWrapper : CallAdapter<R, Observable<R>> {
//        val retrofit: Retrofit
//        val wrapper: CallAdapter<R, R>
//
//        constructor(retrofit1: Retrofit, wrapper2: CallAdapter<R, R>) {
//            retrofit = retrofit1
//            wrapper = wrapper2
//        }
//
//        override fun responseType(): Type {
//            return wrapper.responseType()
//        }
//
//        override fun adapt(call: Call<R>?): Observable<R> {
//            val observable : Observable<R> = wrapper.adapt(call) as Observable<R>
//            observable.onErrorResumeNext {
//
//            }
//
//            return observable
//        }
//    }

}