package com.hap.sunshine.dagger2.annotation.module

/**
 * Created by elpolvo on 6/8/17.
 */
@dagger.Module
class GsonModule {
    @dagger.Provides
    @javax.inject.Singleton
    fun provideGson(): com.google.gson.Gson = com.google.gson.GsonBuilder().create()
}