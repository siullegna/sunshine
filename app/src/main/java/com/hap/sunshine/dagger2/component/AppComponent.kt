package com.hap.sunshine.dagger2.component

import com.hap.sunshine.MainActivity
import com.hap.sunshine.SunshineApplication
import com.hap.sunshine.dagger2.annotation.module.AppComponentModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by elpolvo on 6/8/17.
 */
@Singleton
@Component(modules = arrayOf(
        AppComponentModule::class
//        ,
//        ClientModule::class,
//        GsonConverterModule::class,
//        GsonModule::class,
//        HostModule::class,
//        NetworkModule::class
))
interface AppComponent {
    fun inject(sunshineApplication: SunshineApplication)
    fun inject(mainActivity: MainActivity)
}