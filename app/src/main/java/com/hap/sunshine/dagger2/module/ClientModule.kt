package com.hap.sunshine.dagger2.annotation.module

/**
 * Created by elpolvo on 6/8/17.
 */
@dagger.Module
class ClientModule {

    @dagger.Provides
    @javax.inject.Singleton
    fun provideClient(networkTimeoutSecond: Long, logger: okhttp3.logging.HttpLoggingInterceptor): okhttp3.OkHttpClient {

        val okHttpClientBuilder: okhttp3.OkHttpClient.Builder = okhttp3.OkHttpClient.Builder()
        okHttpClientBuilder.readTimeout(networkTimeoutSecond, java.util.concurrent.TimeUnit.SECONDS)
        okHttpClientBuilder.connectTimeout(networkTimeoutSecond, java.util.concurrent.TimeUnit.SECONDS)

        if (com.hap.sunshine.BuildConfig.DEBUG) {

            logger.level = okhttp3.logging.HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(logger)
        }

        return okHttpClientBuilder.build()

    }

}