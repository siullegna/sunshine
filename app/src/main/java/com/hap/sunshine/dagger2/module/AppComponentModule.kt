package com.hap.sunshine.dagger2.annotation.module

import android.content.Context
import android.content.res.Resources
import com.hap.sunshine.SunshineApplication
import com.hap.sunshine.dagger2.annotation.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by elpolvo on 6/8/17.
 */
@Module
class AppComponentModule(private val application: SunshineApplication) {
    @Provides
    @Singleton
    @ApplicationScope
    fun provideApplicationContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideResources(): Resources = application.resources
}