package com.hap.sunshine.dagger2.annotation.module

import com.hap.sunshine.network.WeatherRestApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by elpolvo on 6/8/17.
 */
@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient, baseUrl: String, converter: Converter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converter)
                .client(httpClient)
                .build()
    }

    @Provides
    @Singleton
    fun provideService(retrofit: Retrofit): WeatherRestApi {
        return retrofit.create(WeatherRestApi::class.java)
    }

    @Provides
    @Singleton
    fun providesWeatherService(): String {
        return "Some example"
    }
}