package com.hap.sunshine.dagger2.annotation.module

/**
 * Created by elpolvo on 6/8/17.
 */
@dagger.Module
class GsonConverterModule {
    @dagger.Provides
    @javax.inject.Singleton
    fun provideParser(gson: com.google.gson.Gson): retrofit2.Converter.Factory = retrofit2.converter.gson.GsonConverterFactory.create(gson)
}