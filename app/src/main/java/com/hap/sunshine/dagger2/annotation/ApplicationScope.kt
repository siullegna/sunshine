package com.hap.sunshine.dagger2.annotation

import javax.inject.Qualifier

/**
 * Created by elpolvo on 6/17/17.
 */
@Qualifier
annotation class ApplicationScope