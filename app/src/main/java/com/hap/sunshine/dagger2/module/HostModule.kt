package com.hap.sunshine.dagger2.annotation.module

/**
 * Created by elpolvo on 6/8/17.
 */
@dagger.Module
class HostModule {
    @dagger.Provides
    @javax.inject.Singleton
    fun provideBaseUrl(): String = com.hap.sunshine.settings.Host.WEATHER_HOST
}