package com.hap.sunshine

import android.content.res.Resources
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import butterknife.BindView
import butterknife.ButterKnife
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @BindView(R.id.forecast_list)
    internal var forecastList: RecyclerView? = null

    @Inject
    lateinit var res: Resources

//    @Inject
//    lateinit var string: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        (application as SunshineApplication).component.inject(this)

        Log.d("String --->", "string: " + res.getString(R.string.app_name))

//        try {
//            weatherRestService.getWeather(7, 98838, "MX", "metric")
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(Schedulers.newThread())
//                    .subscribe()
//        } catch (e: Exception) {
//            Log.e("TAG", e.message)
//        }
    }
}
